# ORID

## O

1. Learning the React Router
2. Axios
3. Promise
4. AntDesign Usage

## R

full

## I

By learning how to use these tools, I have gained a deeper understanding of the React application development process
and best practices.
Mastering these tools will help me develop React projects faster and better, and write componentized and reusable code.

## D

I will continue to learn more tools and techniques from the React ecosystem.
I will practice more and apply what I have learned in real projects to write higher quality code.
