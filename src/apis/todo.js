import api from "./api"

export const getTodoList = () => {
  return api.get('/todo')
}

export const getTodoById = (id) => {
  return api.get(`/todo/${id}`)
}

export const updateTodo = (id, todo) => {
  return api.put(`/todo/${id}`, todo)
}

export const deleteTodo = (id) => {
  return api.delete(`/todo/${id}`)
}

export const addTodo = (todo) => {
  return api.post(`/todo`, todo)
}
