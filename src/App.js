import './App.css';
import {Outlet, Link} from "react-router-dom";
import {Layout, Menu} from 'antd';
import {HomeOutlined, FileDoneOutlined, SmileOutlined} from "@ant-design/icons";
import {useState} from "react";

const {
  Content,
  Header,
  Footer
} = Layout;


function App() {
  const [current, setCurrent] = useState('mail');

  const items = [
    {
      label: <Link to="/">Home</Link>,
      key: 'Home',
      icon: <HomeOutlined/>,
    },
    {
      label: <Link to="/done">Done List</Link>,
      key: 'DoneList',
      icon: <FileDoneOutlined/>,
    },
    {
      label: <Link to="/help">Help</Link>,
      key: 'Help',
      icon: <SmileOutlined/>,
    },
  ]

  const onClick = (e) => {
    setCurrent(e.key);
  };
  return (
    <Layout>
      <Header className="app-header">
        <Menu onClick={onClick} selectedKeys={[current]} mode="horizontal" items={items}/>
      </Header>
      <Content><Outlet/></Content>
      <Footer></Footer>
    </Layout>
  );
}

export default App;
