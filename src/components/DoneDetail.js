import {useParams} from "react-router-dom";
import {useSelector} from "react-redux";

export const DoneDetail = () => {
  const {id} = useParams();
  const todoList = useSelector(state => state.todolist.todoList.find(todo => todo.id === id));

  return (
    <div>
      <h1>Done Detail</h1>
      <div>ID: {todoList?.id}</div>
      <div>TEXT: {todoList?.text}</div>
    </div>
  )
}
