import "./TodoList.css"
import {Card, Modal, message, Input} from "antd"
import {CloseOutlined, EditOutlined, EyeOutlined} from "@ant-design/icons";
import {useState} from "react";
import {useTodos} from "../hooks/useTodo";
import {Col, Row} from "antd";

export const TodoItem = (props) => {
  const {todo} = props
  const [newText, setNewText] = useState('')
  const [todoFromDb, setTodoFromDb] = useState({})
  const [showDeleteModal, setShowDeleteModal] = useState(false)
  const [showEditModal, setShowEditModal] = useState(false)
  const [showDetailModal, setShowDetailModal] = useState(false)

  const {updateTodo, deleteTodo, loadTodoById} = useTodos()


  const handleDelete = async () => {
    await deleteTodo(todo.id)
    message.open({type: 'success', content: 'delete success',});
  }

  const handleUpdateDoneStatus = async () => {
    await updateTodo(todo.id, {text: todo.text, done: !todo.done});
    message.open({type: 'success', content: 'update success',});
  }


  const handleInputChange = (e) => {
    setNewText(e.target.value === '' ? ' ' : e.target.value.trim())
  }

  const handleModify = async () => {
    await updateTodo(todo.id, {text: newText, done: todo.done})
    message.open({type: 'success', content: 'update success',});
    setShowEditModal(false)
  }

  const handleEyeOutlinedClick = async () => {
    setShowDetailModal(true)
    const todoFromDb = await loadTodoById(todo.id)
    setTodoFromDb(todoFromDb)
  }

  return (
    <div className="todo-item">
      <Card className={`todo-item-${todo.done ? 'del-' : ''}card`}>
        <Row justify="space-between" className="todo-item-text">
          <Col flex="auto"
               onClick={handleUpdateDoneStatus}>{todo.text?.length > 15 ? todo.text?.slice(0, 15).concat('...') : todo.text}</Col>
          <Col>
                        <span className='close-icon'>
                            <CloseOutlined onClick={() => setShowDeleteModal(true)}/>
                        </span>
            <span className='edit-icon'>
                            {!todo.done && <EditOutlined onClick={() => setShowEditModal(true)}/>}
                        </span>
            <span className='info-icon'>
                            {<EyeOutlined onClick={handleEyeOutlinedClick}/>}
                        </span>
          </Col>
        </Row>

      </Card>

      {/* delete modal */}
      <Modal
        open={showDeleteModal}
        onOk={handleDelete}
        onCancel={() => setShowDeleteModal(false)}
      >
        confirm delete : {todo.text} ?
      </Modal>

      {/* edit modal */}
      <Modal
        open={showEditModal}
        onOk={handleModify}
        onCancel={() => setShowEditModal(false)}
      >
        <div>
          <div className="todo-item-edit-span">
            editing : {todo.text}
          </div>
          <Input
            value={newText || todo.text}
            onChange={handleInputChange}
            onPressEnter={handleModify}
          />
        </div>
      </Modal>

      {/* info modal */}
      <Modal
        open={showDetailModal}
        onOk={() => setShowDetailModal(false)}
        onCancel={() => setShowDetailModal(false)}
      >
        <strong>Info</strong>
        <p><strong>id:</strong> {todoFromDb.id}</p>
        <p><strong>text:</strong> {todoFromDb.text}</p>
        <p><strong>done: </strong> {todoFromDb.done ? 'true' : 'false'}</p>
      </Modal>
    </div>
  )
}
