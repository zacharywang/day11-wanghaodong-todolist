import {useSelector} from "react-redux";
import {DoneItem} from "./DoneItem";

export const DoneGroup = () => {
  const todoList = useSelector(state => state.todolist.todoList.filter(todo => todo.done));
  return (
    <div>
      {
        todoList?.map((todo) => {
          return <DoneItem todo={todo} key={todo.id}/>
        })
      }
    </div>
  )
}
