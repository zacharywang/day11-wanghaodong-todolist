# ORID

## O
1. This morning I did a code review of the todolist for react, and I found some deficiencies in my code.
   For example, when doing component traversal display, to set the unique key value. The main reasons for this are as follows:
   - Optimize performance: React can use the key value to compare the old and new component trees. Without a key value, React may perform more DOM operations, affecting performance.
   - Maintaining component state: Each component should have its own unique identifier so that the component state is correctly preserved on re-rendering.
   - Avoid warnings.
2. and then followed the teacher to review the process of using react yesterday to deepen the impression of the use of react.
3. then studied redux and learned about the introduction and use of redux. Finally, we improved the code of day11.

## R

full

## I
I think redux and vuex in vue are the same, both are for unified state data management. The learning process is also something that needs to be compared and contrasted so that you can become proficient in using both.
I think the most meaningful part of this course is the todolist case, from the use of props to pass values, step by step evolution to the use of redux to save the data, can let us understand the reason, better understand the meaning of its existence.
better understand the meaning of its existence, and its development ideas.

## D
I hope that in the future development process, I can skillfully use redux for state management and save data. During my internship, I have been using class components to develop, but later I realized that the performance of class components is not as good as function components.
The performance of the class component is not as good as the function component, but the class component adopts an object-oriented thought, while the function component is a process-oriented thought, so I think, as a Java programmer, the difficulty of getting started with the class component is much higher than that of the function component.
Therefore, I feel that, as a Java programmer, it is less difficult to get started with class components than with function components. But in the future, I will use function components to change my old way of thinking.
