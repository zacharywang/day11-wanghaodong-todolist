# ORID

## O

Today's learning covered several topics including code review, SpringBoot, HTML, CSS, and React.In the morning, we conducted a code review and discussed issues related to object transfers such as DTO and VO. We also learned about SpringBoot and how it relates to SpringMVC. The afternoon session focused on HTML and CSS, where we learned about CSS selectors and practiced related skills. Finally, we took a deep dive into React and learned how to pass parameters between parent and child components and concepts such as state elevation.

## R

full

## I

Today was challenging, but I gained a lot of useful knowledge. I found the concept of passing parameters between React components particularly useful.

## D

After class, I plan to scrutinize the presentation provided by my teacher and practice what I learned today through homework.